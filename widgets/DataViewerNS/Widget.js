// Title: Data Viewer Widget
// Date: July 19, 2018
// By: Alexander Wojcik // BlueJack Consulting, Inc.

/*global define, console, setTimeout*/
define([
  //dojo
  "dojo/_base/declare",
  "jimu/BaseWidget",
  "jimu/LayerInfos/LayerInfos",
  "jimu/LayerStructure",
  "dijit/_WidgetsInTemplateMixin",
  "dijit/form/Button",
  "dojo/on",
  "dojo/aspect",
  "dijit/form/SimpleTextarea",
  "dojo/dom-class",
  "dojo/_base/array",
  "dojo/string",
  "dojo/_base/lang",
  "dojo/Deferred",
  'dojo/promise/all',

  //esri
  'jimu/WidgetManager',
  'jimu/PanelManager',
  'esri/config',
  "esri/InfoTemplate",
  "esri/geometry/Point",
  "esri/geometry/Extent",
  "esri/tasks/query",
  "esri/tasks/QueryTask",
  "esri/symbols/SimpleLineSymbol",
  "esri/symbols/SimpleFillSymbol",
  "esri/symbols/SimpleMarkerSymbol",
  "esri/layers/FeatureLayer",
  "esri/request",
  "esri/Color",
  "dojo/domReady!"
],
  function (
    //dojo
    declare, BaseWidget, LayerInfos, LayerStructure, _WidgetsInTemplateMixin, Button, on, aspect, SimpleTextarea,
    domClass, array, dojoString, lang, Deferred, all,

    //esri
    WidgetManager, PanelManager, esriConfig, InfoTemplate, Point, Extent, Query, QueryTask,
    SimpleLineSymbol, SimpleFillSymbol, SimpleMarkerSymbol, FeatureLayer, esriRequest, Color
  ) {

    //To create a widget, you need to derive from BaseWidget.
    return declare([BaseWidget, _WidgetsInTemplateMixin], {

      // this property is set by the framework when widget is loaded.
      baseClass: 'data-viewer-ns',
      name: "DataViewerNS",
      label: "DataViewerNS",

      queryTask: null,
      layerQuery: null,
      serviceQuery: null,
      featureLayer: null,
      layerInfo: null,
      queryLayers: [],
      currentQueryLayer: "",
      addedLayers: [],
      queryFields: ["AssetCode", "AssetSubtype", "FeatureCode"],

      // loop through layer tree, add any url ending in a digit + layer name
      // to this.queryLayers array.
      getQueryLayers: function () {

        console.info("Run: getQueryLayers")

        var layerStructure = LayerStructure.getInstance();

        layerStructure.traversal(lang.hitch(this, function (layerNode) {

          for (var i = 0; i < layerNode.getNodeLevel(); i++) {

            var lUrl = layerNode._layerInfo.layerObject._url.path;
            var lName = layerNode._layerInfo.layerObject.name;
            var lastChar = lUrl.substr(lUrl.length - 1);
            var hasNumber = /\d/;
            var isURLALayer = hasNumber.test(lastChar);

            if (isURLALayer) {
              var layerObject = {
                "url": lUrl,
                "name": lName
              };
              this.queryLayers.push(layerObject);
            }

          }
        }));
      },

      clearMap: function () {

        console.log("Run: clearMap");

        for (var featureLayer = 0; featureLayer < this.addedLayers.length; featureLayer++) 
        {
          this.map.removeLayer(this.addedLayers[featureLayer]);
        }

        this.addedLayers = [];
        this.featureLayer = null;

      },

      postCreate: function () {
        this.inherited(arguments);
      },

      startup: function startupFunc() {

        console.info("Run: startup");

        this.inherited(arguments);

        // if user clicks on "Clear Query" button
        this.own(on(this.btnClearQueryBoxNode, "click", lang.hitch(this, function () {
          console.log("Run: clearQuery");
          this.myarea.set("value", "");
        })));

        // if user clicks on "Search" button
        this.own(on(this.btnSearchNode, "click", lang.hitch(this, this.handlerExecQuery)));

        // if user clicks on "Clear Map" button
        this.own(on(this.btnClearGfxNode, "click", lang.hitch(this, this.clearMap)));

        // get layers to be queried from map
        this.getQueryLayers();

        this.query = new Query();
        this.query.returnGeometry = true;
        this.query.outSpatialReference = this.map.spatialReference;
        this.query.outFields = ['*'];
      },

      requestSucceeded: function (response) {

        this.layerInfo = response;
        if (response.hasOwnProperty("fields")) {
          this.selectFieldNode.options.length = 0;
          var fieldInfoName = array.map(response.fields, function (f) {
            return f.name;
          });
          var options = [];
          /////populate field dropdown menu
          for (var i = 0; i < fieldInfoName.length; i++) {
            var option = {
              value: fieldInfoName[i],
              label: fieldInfoName[i]
            };
            options.push(option);
          }
          this.selectFieldNode.addOption(options);
          this.selectFieldNode.selectedIndex = 1;
          this.optionFieldSel = this.selectFieldNode.get('value');
        } else {
          console.log("No field info found. Please double-check the URL.");
        }
      },

      requestFailed: function (error) {
        console.log("Getting Layer List JSON failed", error);
      },

      requestFailedLayerList: function () {
        console.log("Failed to get layer list. ");
      },

      ///exec query Search button
      handlerExecQuery: function () {

        console.info("Run: handlerExecQuery");

        this.clearMap();

        if (this.queryLayers.length > 0)
        {
          for (var layer = 0; layer < this.queryLayers.length; layer++) {
            var lQueryURL = this.queryLayers[layer].url;
            var lCurrentQueryLayer = this.queryLayers[layer].name;
  
            this.queryTask = new QueryTask(lQueryURL);
            lang.hitch(this, this.executeQueryTask(lCurrentQueryLayer));
          }
        }

        else 
        {
          console.info("Error: No layers to query.");
        }

      },

      executeQueryTask: function (currentQueryLayer) {
        //set query based on what user typed in ;

        console.info("Run: executeQueryTask");

        // for queryLayer perform seperate queries on the different fields defined in
        // this.queryFields
        for (var field = 0; field < this.queryFields.length; field++) 
        {
          this.query.where = this.queryFields[field] + " LIKE '" + this.myarea.value + "'";
          this.queryTask.execute(this.query, lang.hitch(this, this.showResults(currentQueryLayer)));
        }

      },

      calcGraphicsExtent: function (graphicsArray) {

        console.info("Run: calcGraphicsExtent");

        var g = graphicsArray[0].geometry,
          fullExt = g.getExtent(),
          ext, i, il = graphicsArray.length;

        if (fullExt === null) {
          fullExt = new Extent(g.x, g.y, g.x, g.y, g.spatialReference);
        }
        for (i = 1; i < il; i++) {
          ext = (g = graphicsArray[i].geometry).getExtent();
          if (ext === null) {
            ext = new Extent(g.x, g.y, g.x, g.y, g.spatialReference);
          }
          fullExt = fullExt.union(ext);
        }
        return fullExt;
      },


      showResults: function (currentQueryLayer) {

        //console.info("Run: showResults");

        return function (featureSet) {
          var resultFeatures = featureSet.features;

          if (resultFeatures.length === 0) {
            //console.log("No results found.")
          } else {
            //this.map.graphics.clear();
            var extent = this.calcGraphicsExtent(resultFeatures);
            this.map.setExtent(extent); //zoom to query results

            var geometryTypeQuery = featureSet.geometryType;

            var markerSymbolQuery = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 11,
              new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color([255, 0, 0]), 1),
              new Color([111, 211, 55, 1]));

            var lineSymbolQuery = new SimpleLineSymbol(
              SimpleLineSymbol.STYLE_DASH,
              new Color([255, 0, 0]),
              3);

            var polygonSymbolQuery = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
              new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color([222, 0, 0]), 1), new Color([255, 255, 0, 0.25])
            );

            var symbolQuery;
            switch (geometryTypeQuery) {
              case "esriGeometryPoint":
                symbolQuery = markerSymbolQuery;
                break;
              case "esriGeometryPolyline":
                symbolQuery = lineSymbolQuery;
                break;
              case "esriGeometryPolygon":
                symbolQuery = polygonSymbolQuery;
                break;
              default:
                symbolQuery = polygonSymbolQuery;
            }

            //create a feature collection
            var featureCollection = {
              "layerDefinition": null,
              "featureSet": {
                "features": [],
                "geometryType": geometryTypeQuery
              }
            };

            featureCollection.layerDefinition = {
              "geometryType": geometryTypeQuery,
              "drawingInfo": {
                "renderer": {
                  "type": "simple",
                  "symbol": symbolQuery.toJson()
                }
              },
              "fields": featureSet.fields
            };

            var infoTemplateQuery = new InfoTemplate("Details", "${*}"); // wildcard for all fields.  Specifiy particular fields here if desired.
            //create a feature layer based on the feature collection

            this.featureLayer = new FeatureLayer(featureCollection, {
              id: resultFeatures.length + " result(s) for '"+ this.myarea.value + "' in " + currentQueryLayer,
              infoTemplate: infoTemplateQuery
            });

            this.map.addLayer(this.featureLayer);
            this.addedLayers.push(this.featureLayer);

            //loop though the features
            for (var i = 0, len = resultFeatures.length; i < len; i++) {
              var feature = resultFeatures[i];
              feature.setSymbol(symbolQuery);
              this.featureLayer.add(feature);
            }
            this._openResultInAttributeTable(this.featureLayer);
          }
        };
      },

      onLayerInfosChanged: function (layerInfo, changeType, layerInfoSelf) {
        if (!layerInfoSelf || !layerInfo) {
          return;
        }
        if ('added' === changeType) {
          layerInfoSelf.getSupportTableInfo().then(lang.hitch(this, function (supportTableInfo) {
            if (supportTableInfo.isSupportedLayer) {
              this.publishData({
                'target': 'AttributeTable',
                'layer': layerInfoSelf
              });
            }
          }));
        } else if ('removed' === changeType) {
          // do something
        }
      },

      _openResultInAttributeTable: function () {

        console.info("Run: _openResultInAttributeTable");

        LayerInfos.getInstance(this.map, this.map.itemInfo).then(lang.hitch(this, function (layerInfosObj) {
          this.own(layerInfosObj.on(
            'layerInfosChanged',
            lang.hitch(this, this.onLayerInfosChanged)));
        }));

      }
    });
  });
