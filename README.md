# README #

* Title: DMA Widget Development Folder
* Created by: BlueJack Consulting Inc. 
* Date: July 24, 2018
* Description: Repo of widget dev folder from my local machine.

### What is this repository for? ###

* This repo contains custom widgets developed for the DMA WAB App
* As of June 24, 2018 the contents consist of the "DataViewerNS" widget

### How do I get set up? ###

* WAB 2.8 must be installed.
* Requires: npm, yeoman, esri-appbuilder-js, esri-widget, gulp 
* Follow directions in https://bitbucket.org/rsutcliffe/wab_testspace/src/master/README.md to configure your environment such that you have a widget dev folder that becomes automatically sync'd with the DMA WAB App with the use of gulp 

### Who do I talk to? ###

* Repo owner: Alexander Wojcik (awojcik@bluejack.ca / alexdouglaswojcik@gmail.com)
* Other community or team contact: Rachel O'Neil (roneil@bluejack.ca)